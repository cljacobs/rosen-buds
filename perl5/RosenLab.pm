=head1 NAME

RosenLab - Base module for all Rosen Lab Perl-related stuff.

=head1 SYNOPSIS

	use RosenLab;

=head1 DESCRIPTION

The module serves as a common root for various other RosenLab modules. It
contains a number of utility functions that are of general use throughout the
RosenLab namespace.

=cut

# module stuff
package RosenLab;
BEGIN
{
	our $VERSION = '1.000'; # use string for MakeMaker (and similar)
	$VERSION = eval "$VERSION"; # convert to number for `use` (and similar)
	use Exporter qw< import >;
	our @ISA = qw< Exporter >;
	our @EXPORT = qw<  >;
	our @EXPORT_OK =
		qw<
			build_command
			build_command_options
			build_libpath
			filter
			getopt
			getopt_filter
		>;
	our %EXPORT_TAGS =
		(
			'ALL' => [@EXPORT,@EXPORT_OK],
		);
}

# pragmas
use 5.030;
use strict; use warnings; use warnings::register;
use experimental qw< signatures >;

# core modules
use English;
use File::Spec::Functions qw< catdir rootdir >;
use Getopt::Long;
use List::Util qw< reduce pairmap >;
use Scalar::Util qw< blessed looks_like_number reftype >;

# # cpan modules
use Lingua::EN::Inflect qw< PL >;

# custom modules
use RosenLab::Log::Simple;

=head1 CONSTANTS

=over

=item ROOT

The absolute path of the Rosen Lab shared space. This location serves as the
I<de facto> root path for the vast majority of Rosen Lab tasks (data storage,
analysis, etc.). Hence, the name.

=item SOFTWATE

The absolute path to the base software repository.

=back

=cut

use constant ROOT => catdir(rootdir(),'broad','rosenlab_archive');

use constant SOFTWARE => catdir(ROOT,'Software');

=head1 FUNCTIONS

=cut

=head2 File I/O functions

=cut

=head3 open_readable

	open_readable($FILENAME)

Opens a file for reading. Kills the program if not successful.

=cut

sub open_readable ( $filename )
{
	debug("Opening file for reading: ${filename}");
	fatal("File does not exist: ${filename}") unless (-f $filename);
	fatal("Cannot read from file (permission denied): ${filename}")
		unless (-r $filename);
	return IO::File->new($filename,'<');
}

=head3 open_writable

	open_writable($FILENAME)
	open_writable($FILENAME,$APPEND)

Opens a file for writing. By default, files will be I<appended> if they already
exist. Kills the program if not successful.

=cut

sub open_writable ( $filename , $append = 1 )
{
	debug("Opening file for writing: ${filename}");
	my $dirname = dirname($filename);
	fatal("Cannot create file in non-existent directory: ${dirname}")
		unless (-d $dirname);
	fatal("Cannot create file (permission denied): ${dirname}")
		unless (-w $dirname);
	if ($append)
	{
		warning("File already exists: ${filename}") if (-f $filename);
		return IO::File->new($filename,'>>');
	}
	fatal("File already exists: ${filename}") if (-f $filename);
	return IO::File->new($filename,'>');
}

=head2 General utility functions

=cut

=head3 filter

	filter([ OBJECTS ],{ KEY => $VALUE , ... })
	filter([ OBJECTS ],{ KEY => $VALUE , ... },$REDUCE,$DEFAULT,$NAME,$SINGULAR)

Returns a sublist of objects (C<[ OBJECTS ]>) that meet the provided selection
criteria (C<{ KEY => VALUE }>). Keys must match a valid attribute of the object
list. Currently, this function only supports three criterion "types": numbers
and strings, which must match exactly, and regular expressions, which are
evaluated against the key (as per C<m//>).

The C<$REDUCE> argument may be used to modify the behavior by which multiple
selection criteria are "combined". This must be a subroutine that accepts two
Boolean inputs and reduces them over the desired operation (as per the namesake
function from L<List::Util>. Here is an example, which will return a list of all
objects "owned" by 'Me' I<or> whose name contains the word 'mouse'
(case-insensitive matching):

	my $reduce = sub ($this,$that) { return ($this or $that); }
	find(\@objects,{ owner => 'Me' , name => qr<(?i)mouse> },$reduce)

The C<$DEFAULT> argument may be used to specify whether or not an empty value
should default to a "match" or not. By default, this value is false.

The C<$NAME> argument is used only by the debugging/logging functions in this
subroutine. When provided, messages will be explicit about the types of objects
they are filtering. By default, this is simply "object".

The C<$SINGULAR> argument is used in scalar context to assert that only a single
result is found. By default, the I<first> result found is returned, regardless
of the number of objects passing the filter(s). I'm not convinced this argument
is actually a good idea. It may disappear in the future release.

=cut

sub filter ( $objects , $criteria , $reduce = sub ($a,$b) { return ($a and $b) } , $default = 0 , $name = 'object' , $singular = 0 )
{
	my @criteria = keys $criteria->%*;
	my $names = PL($name);
	debug("Filtering ${names} by criteria: @{criteria}");
	my @found = grep
		{
			my $finding = $_;
			my $return = reduce
				{
					$reduce->($a,$b);
				} pairmap
					{
						_filter_single_object($finding,$a,$b,$default);
					} $criteria->%*;
		} $objects->@*;
	# return the filtered list unless only a single finding is required
	return @found if (wantarray);
	if ($singular)
	{
		debug("Assuring that exactly one ${name} matched the filter criteria.");
		fatal("No ${names} match provided criteria.") unless (@found);
		fatal("Multiple ${names} match the provided criteria.")
			unless ($#found == 0);
	}
	return $found[0];
}

sub _filter_single_object ( $finding , $attribute , $value , $default = 0 )
{
	return $default unless (defined $value);
	if (blessed($value) && (reftype($value) eq 'REGEXP'))
	{
		return scalar $finding->$attribute =~ m<$value>;
	}
	return looks_like_number($value)
		? $finding->$attribute == $value
		: $finding->$attribute eq $value;
}

=head3 getopt

	getopt($VALID,$ARGV)
	getopt($VALID,$ARGV,$FATAL,$ERROR)

Parses valid (C<$VALID>) options out of an array of arguments (C<$ARGV>). This
function acts as a wrapper around the C<Getopt::Long> module. It provides some
standard templating that generates a POSIX-like Getopt object (parser) with
customizable error handling.

The C<$FATAL> argument is a function reference that handles a failure of the
option parser. This error message is generated at the end of option parsing,
after all relevant errors have already been logged.

The C<$ERROR> argument is a function reference that handles error messages
generated while parsing options. Errors messages are generated whenever an
unknown or invalid option is encountered in the options list.

=cut

sub getopt ( $valid , $argv , $fatal = \&fatal , $error = \&error )
{
	my $option_parser = Getopt::Long::Parser->new();
	$option_parser->configure('bundling','require_order');
	my @noptions = ();
	# catch the unknown options warnings and turn into (logged) errors
	$SIG{__WARN__} = sub ( $message )
		{
			chomp $message;
			if ($message =~ m(^Unknown option: (?<option>\S+)$))
			{
				# message is about an unknown options
				my $option = $LAST_PAREN_MATCH{'option'};
				$error->("Encountered unknown <option>: ${option}");
				push @noptions , "--${option}";
			}
			elsif ($message =~ m(^Option (?<option>\S+) requires an argument$))
			{
				# warning is about missing option argument
				my $option = $LAST_PAREN_MATCH{'option'};
				$error->("Missing required argument for <option>: ${option}");
				push @noptions , "--${option}";
			}
			elsif ($message =~ m(^Value "(?<value>\S+)" invalid for option (?<option>\S+)))
			{
				# warning is about invalid option argument
				my $option = $LAST_PAREN_MATCH{'option'};
				$error->("Invalid argument for <option>: ${option}");
				push @noptions , "--${option}";
			}
			else
			{
				fatal("Could not parse option: ${message}")
			}
		};
	# get the options
	$option_parser->getoptionsfromarray(
		$argv, # the arguments array to parse for valid options
		$valid->%*, # input/output control
		) or $fatal->("Errors parsing command arguments (invalid options): @noptions");
	# return the unrecognized options
	return @noptions;
}

=head3 getopt_filter

	getopt_filter($CLASS,$ARGV)
	getopt_filter($CLASS,$ARGV,$FATAL,$ERROR)

Parses a common set of options to search/filter objects based on a set of
criteria. Returns a hash reference containing the filtering criteria, plus a
scalar "Boolean" indicating case-sensitive matching.

=cut

sub getopt_filter ( $class , $argv , $fatal = \&fatal , $error = \&error )
{
	my $criteria = {};
	my $sensitive = 0;
	my %parser = map
		{
			my $attribute = $_;
			$attribute . "=s" => sub ( $id , $value ) { $criteria->{$id} = $value; }
		} Mouse::Meta::Class->initialize($class)->get_attribute_list;
	$parser{'case-sensitive'} = \$sensitive;
	my @options = getopt(\%parser,$argv,$fatal,$error);
	return ($criteria,$sensitive);
}

=head1 SEE ALSO

L<RosenLab::Data>, L<RosenLab::Project>, L<RosenLab::Pipeline>

=head1 COPYRIGHT & LICENSE

Copyright (C) 2018 by Christopher Jacobs

Permission to use, copy, modify, and/or distribute this software for any purpose
with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF
THIS SOFTWARE.

=cut

1;

__END__

=head1 NAME

RosenLab::Data - An interface to Rosen Lab data.

=head1 SYNOPSIS

	use RosenLab::Data;

=head1 DESCRIPTION

The module serves mainly as a common base for various other RosenLab modules
relating to raw data. There's not much to this module at the moment. It only
contains a few useful constants.

=cut

# module stuff
package RosenLab::Data;
BEGIN
{
	our $VERSION = '1.000'; # use string for MakeMaker (and similar)
	$VERSION = eval "$VERSION"; # convert to number for `use` (and similar)
	use Exporter qw< import >;
	our @ISA = qw< Exporter >;
	our @EXPORT = qw<  >;
	our @EXPORT_OK = qw<  >;
	our %EXPORT_TAGS = ();
}

# pragmas
use 5.030;
use strict; use warnings;
use experimental qw< signatures >;

# core modules
use File::Spec::Functions qw< catdir >;

# custom modules
use RosenLab;

=head1 CONSTANTS

=over

=item REPOSITORY

The absolute path of the base Rosen Lab data repository. This location is used
as the fallback location for data types that do not have thier own repository
locations defined.

=back

=cut

use constant REPOSITORY => catdir(RosenLab::ROOT,'Data');

use constant TECHNOLOGIES =>
	qw<
		ATAC-Seq
		ChAP-Seq
		ChIP-Seq
		Chromium-v3
		DIP-Seq
		Drop-Seq
		DroNc-Seq
		RNA-Seq
	>;

=head1 SEE ALSO

L<RosenLab>, L<RosenLab::Data::Sequencing>, L<RosenLab::Data::Genome>, L<RosenLab::Data::Motif>

=head1 COPYRIGHT & LICENSE

Copyright (C) 2018 by Christopher Jacobs

Permission to use, copy, modify, and/or distribute this software for any purpose
with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF
THIS SOFTWARE.

=cut

1;

__DATA__

__END__

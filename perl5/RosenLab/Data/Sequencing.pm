=head1 NAME

RosenLab::Data::Sequencing - An interface to Rosen Lab sequencing data.

=head1 SYNOPSIS

	use RosenLab::Data::Sequencing;
	
	# get the list of sequencers used in any Rosen Lab experiment
	my @sequencers = RosenLab::Data::Sequencing::sequencers();
	
	# get list of sequencing data
	my $run = RosenLab::Data::Sequencing::runs({ name => "Template" });
	my @runs = RosenLab::Data::Sequencing::runs({ year => "2019" });

=head1 DESCRIPTION

Tools for querying the Rosen Lab sequencing data repository.

=cut

# module stuff
package RosenLab::Data::Sequencing;
BEGIN
{
	our $VERSION = '1.000'; # use string for MakeMaker (and similar)
	$VERSION = eval "$VERSION"; # convert to number for `use` (and similar)
	use Exporter qw< import >;
	our @ISA = qw< Exporter >;
	our @EXPORT = qw<  >;
	our @EXPORT_OK = qw< runs sequencers >;
	our %EXPORT_TAGS = ();
}

# pragmas
use 5.030;
use strict; use warnings;
use experimental qw< signatures >;

# core modules
use File::Spec::Functions qw< catdir >;

# cpan modules
use YAML::PP qw<>;

# custom modules
use RosenLab;
use RosenLab::Data;

=head1 CONSTANTS

=over

=item REPOSITORY

The absolute path to the Rosen Lab sequencing data repository. This location is
used by all C<RosenLab::Data::Sequencing> objects to default their own paths.

=back

=cut

use constant REPOSITORY => catdir(RosenLab::Data::REPOSITORY,'Sequencing');

=begin comment head1 INTERNAL VARIABLES

=over

=item runs

An array reference containing all currently known sequencing runs.

=item sequencers

An array reference containing all currently known sequencers.

=back

=end comment

=cut

my $runs = undef;
my $sequencers = undef;

=head1 FUNCTIONS

=cut

=head3 runs

	runs()
	runs{ KEY => $VALUE , ... },$REDUCE)

Returns a list of Rosen Lab sequencing runs that meet the provided selection
criteria. If called without any arguments, this subroutine returns the complete
list of sequencing runs.

The C<$REDUCE> argument may be used to modify the behavior by which multiple
selection criteria are "combined". This must be a reference to a subroutine that
accepts two Boolean inputs and reduces them over the desired operation (as per
the namesake function from L<List::Util>. Here is an example, which will return
a list of all sequencing runs from the year '2019' I<or> was run on the
sequencer whose ID matches 'M001' (case-insensitive matching):

	my $reduce = sub ($this,$that) { return ($this or $that); }
	runs({ year => '2019' , sequencer => qr<(?i)M001> },$reduce)

=cut

sub runs ( $criteria = {} , $reduce = sub ($a,$b) { return ($a and $b) } )
{
	$runs = RosenLab::Data::Sequencing::Run::import() unless (defined $runs);
	# return entire list if no criteria are supplied
	my @runs = values $runs->%*;
	return @runs unless ($criteria->%*);
	# otherwise filter the runs by the criteria
	return RosenLab::filter(\@runs,$criteria,$reduce,0,"sequencing run");
}

=head3 sequencers

	sequencers()
	sequencers({ KEY => $VALUE , ... },$REDUCE)

Returns a list of Rosen Lab sequencing machines that meet the provided selection
criteria. If called without any arguments, this subroutine returns the complete
list of sequencing machines.

The C<$REDUCE> argument may be used to modify the behavior by which multiple
selection criteria are "combined". This must be a reference to a subroutine that
accepts two Boolean inputs and reduces them over the desired operation (as per
the namesake function from L<List::Util>. Here is an example, which will return
a list of all sequencers whose name ID begins with 'NB' I<or> whose model
name contains 'NextSeq' (case-insensitive matching):

	my $reduce = sub ($this,$that) { return ($this or $that); }
	runs({ id => qr<(?i)^NB> , model => qr<(?i)NextSeq> },$reduce)

=cut

sub sequencers ( $criteria = {} , $reduce = sub ($a,$b) { return ($a and $b) })
{
	$sequencers = RosenLab::Data::Sequencing::Sequencer::import()
		unless(defined $sequencers);
	# return entire list if no criteria are supplied
	my @sequencers = values $sequencers->%*;
	return @sequencers unless ($criteria->%*);
	# otherwise filter the sequencers by the criteria
	return RosenLab::filter(\@sequencers,$criteria,$reduce,0,"sequencer");
}

=head1 RosenLab::Data::Sequencing::Run

Objectionable representation of a (NGS) sequencing run.

=cut

{
	package RosenLab::Data::Sequencing::Run;
	
	use experimental qw< signatures >;
	use namespace::autoclean;
	use overload q<""> => 'stringify';
	
	use File::Spec::Functions qw< catfile catdir >;
	use Time::Piece;
	
	use XML::LibXML;
	
	use Mouse;
	use Mouse::Util::TypeConstraints;
	no warnings qw< experimental::signatures >; # requried by/after Mouse
	
	use constant DATABASE =>
		catfile(RosenLab::Data::Sequencing::REPOSITORY,'runs.yaml');
	
	sub import ()
	{
		my $yaml = YAML::PP::LoadFile(DATABASE);
		for my $name (keys $yaml->%*)
		{
			my $run = $yaml->{$name};
			$run->{'name'} = $name;
			$runs->{$name} = __PACKAGE__->new($run);
		}
		return $runs;
	}
	
	subtype 'Time::Piece'
		=> as 'Object'
		=> where { $_->isa('Time::Piece') };
	
	coerce 'Time::Piece'
		=> from 'Str'
			=> via { Time::Piece->strptime($_,'%Y-%m-%d') };
	
	has 'date' =>
		(
			is => 'ro' ,
			isa => 'Time::Piece' ,
			required => 1 ,
			coerce => 1 ,
		);
	has 'sequencer' =>
		(
			is => 'ro' ,
			isa => 'Str' ,
			required => 1 ,
		);
	has 'flowcell' =>
		(
			is => 'ro' ,
			isa => 'Str' ,
			required => 1 ,
		);
	has 'number' =>
		(
			is => 'ro' ,
			isa => 'Int' ,
		);
	has 'name' =>
		(
			is => 'rw' ,
			isa => 'Str' ,
			lazy_build => 1 ,
		);
	has 'path' =>
		(
			is => 'ro' ,
			isa => 'Str' ,
			lazy_build => 1 ,
		);
	has 'mask' =>
		(
			is => 'ro' ,
			isa => 'Str' ,
			lazy_build => 1 ,
		);
	
	sub stringify : method ( $self , @others )
	{
		my $string = sprintf '%s{ ' , __PACKAGE__ ;
		$string .= join ' , ' , map
			{
				sprintf '%s => %s' , $_ , $self->$_ ;
			} qw< name path date sequencer flowcell >;
		return sprintf '%s }' , $string;
	}
	
	sub _build_mask ( $self )
	{
		my $path = catfile($self->path,"RunInfo.xml");
		return undef unless (-f $path);
		my $xml = XML::LibXML->load_xml(location => $path);
		return join "," , map
				{
					$_->IsIndexedRead eq 'Y'
						? 'I' . $_->NumCycles
						: 'Y' . $_->NumCycles;
				} $xml->findnodes('/RunInfo/Run/Reads/Read');
	}
	
	sub _build_name ( $self )
	{
		my @parts =
			(
				$self->date->strftime("%y%m%d") ,
				$self->sequencer ,
				defined $self->number ? sprintf "%04d" , $self->number  : () ,
				sprintf "FC%s",$self->flowcell ,
			);
		return join '_' , @parts;
	}
	
	sub _build_path ( $self )
	{
		return catdir(RosenLab::Data::Sequencing->REPOSITORY,$self->name);
	}
	
	__PACKAGE__->meta->make_immutable;
}

=head1 RosenLab::Data::Sequencing::Sequencer

Objectionable representation of an (NGS) sequencer.

=cut

{
	package RosenLab::Data::Sequencing::Sequencer;
	
	use experimental qw< signatures >;
	use namespace::autoclean;
	use overload q<""> => 'stringify';
	
	use File::Spec::Functions qw< catfile >;
	
	use Mouse;
	use Mouse::Util::TypeConstraints;
	no warnings qw< experimental::signatures >; # requried by/after Mouse
	
	use constant DATABASE =>
		catfile(RosenLab::Data::Sequencing::REPOSITORY,'sequencers.yaml');
	
	sub import ()
	{
		my $yaml = YAML::PP::LoadFile(DATABASE);
		for my $name (keys $yaml->%*)
		{
			my $sequencer = $yaml->{$name};
			$sequencer->{'name'} = $name;
			$sequencers->{$name} = __PACKAGE__->new($sequencer);
		}
		return $sequencers;
	}
	
	enum 'RosenLab::Data::Sequencing::Sequencer::PLATFORM'
		=> qw< CAPILLARY HELICOS ILLUMINA IONTORRENT LS454 ONT PACBIO SOLID >;
	
	coerce 'RosenLab::Data::Sequencing::Sequencer::PLATFORM'
		=> from 'Str'
			=> via { uc $_; };
	
	has 'id' =>
		(
			is => 'ro' ,
			isa => 'Str' ,
			required => 1 ,
		);
	has 'model' =>
		(
			is => 'ro' ,
			isa => 'Str' ,
			required => 1 ,
		);
	has 'platform' =>
		(
			is => 'ro' ,
			isa => 'RosenLab::Data::Sequencing::Sequencer::PLATFORM' ,
			required => 1 ,
		);
	has 'name' =>
		(
			is => 'rw' ,
			isa => 'Str' ,
			lazy_build => 1 ,
		);
	has 'runs' =>
		(
			is => 'ro' ,
			isa => 'ArrayRef[RosenLab::Data::Sequencing::Run]' ,
			lazy_build => 1 ,
		);
	
	sub stringify : method ( $self , @others )
	{
		my $string = sprintf '%s{ ' , __PACKAGE__ ;
		$string .= join ' , ' , map
			{
				sprintf '%s => %s' , $_ , $self->$_ ;
			} qw< name id model platform >;
		return sprintf '%s }' , $string;
	}
	
	sub _build_name ( $self )
	{
		return $self->id;
	}
	
	sub _build_runs ( $self )
	{
		return [RosenLab::Data::Sequencing::runs({ sequencer => $self->id })];
	}
	
	__PACKAGE__->meta->make_immutable;
}

=head1 COPYRIGHT & LICENSE

Copyright (C) 2018 by Christopher Jacobs

Permission to use, copy, modify, and/or distribute this software for any purpose
with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF
THIS SOFTWARE.

=cut

1;

__DATA__

__END__

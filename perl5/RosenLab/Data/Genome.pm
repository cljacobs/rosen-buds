=head1 NAME

RosenLab::Data::Genome - An interface to Rosen Lab genomic data.

=head1 SYNOPSIS

	use RosenLab::Data::Genome;
	
	# get the list of genome assemblies used in any Rosen Lab experiment
	my @assemblies = RosenLab::Data::Genome::assemblies();
	
	# get the list of annotations regarding the 'mm10' assembly
	my @annotations = RosenLab::Data::Genome::annotations('mm10');

=head1 DESCRIPTION

Tools for querying the Rosen Lab genomic data repository.

=cut

# module stuff
package RosenLab::Data::Genome;
BEGIN
{
	our $VERSION = '1.000'; # use string for MakeMaker (and similar)
	$VERSION = eval "$VERSION"; # convert to number for `use` (and similar)
	use Exporter qw< import >;
	our @ISA = qw< Exporter >;
	our @EXPORT = qw<  >;
	our @EXPORT_OK = qw< annotations assemblies >;
	our %EXPORT_TAGS = ();
}

# pragmas
use 5.030;
use strict; use warnings;
use experimental qw< signatures >;

# core modules
use File::Spec::Functions qw< catdir catfile >;

# cpan modules
use YAML::PP qw<>;

# custom modules
use RosenLab;
use RosenLab::Data;

=head1 CONSTANTS

=over

=item REPOSITORY

The absolute path to the Rosen Lab genomic data repository. This location is
used by all C<RosenLab::Data::Genome> objects to default their own paths.

=back

=cut

use constant REPOSITORY => catdir(RosenLab::Data::REPOSITORY,'Genomes');

=begin comment head1 INTERNAL VARIABLES

=over

=item runs

An array reference containing all currently known sequencing runs.

=item sequencers

An array reference containing all currently known sequencers.

=back

=end comment

=cut

my $assemblies = undef;
my $annotations = undef;

=head1 FUNCTIONS

=cut

=head3 annotations

	annotations()
	annotations({ KEY_1 => $VALUE_1 , ... },$REDUCE)

Returns a list of genome annotations that meet the provided selection criteria.
If called without any arguments, this subroutine returns the complete list of
annotations.

The C<$REDUCE> argument may be used to modify the behavior by which multiple
selection criteria are "combined". This must be a reference to a subroutine that
accepts two Boolean inputs and reduces them over the desired operation (as per
the namesake function from L<List::Util>. Here is an example, which will return
a list of all annotations for the species "Mus musculus" I<or> that were sourced
from "GENCODE":

	my $reduce = sub ($this,$that) { return ($this or $that); }
	annotations({ species => 'Mus musculus' , source => 'GENCODE' },$reduce)

=cut

sub annotations ( $criteria = {} , $reduce = sub ($a,$b) { return ($a and $b) } )
{
	$annotations = RosenLab::Data::Genome::Annotation::import()
		unless (defined $annotations);
	# return entire list if no criteria are supplied
	my @annotations = values $annotations->%*;
	return @annotations unless ($criteria->%*);
	# otherwise filter the runs by the criteria
	return RosenLab::filter(\@annotations,$criteria,$reduce,0,"annotation");
}

=head3 assemblies

	assemblies()
	assemblies({ KEY => $VALUE , ... },$REDUCE)

Returns a list of genome assemblies that meet the provided selection criteria.
If called without any arguments, this subroutine returns the complete list of
assemblies.

The C<$REDUCE> argument may be used to modify the behavior by which multiple
selection criteria are "combined". This must be a reference to a subroutine that
accepts two Boolean inputs and reduces them over the desired operation (as per
the namesake function from L<List::Util>. Here is an example, which will return
a list of all assemblies for the species "Mus musculus" I<or> that were sourced
from "GENCODE":

	my $reduce = sub ($this,$that) { return ($this or $that); }
	assemblies({ species => 'Mus musculus' , source => "GENCODE" },$reduce)

=cut

sub assemblies ( $criteria = {} , $reduce = sub ($a,$b) { return ($a and $b) } )
{
	$assemblies = RosenLab::Data::Genome::Assembly::import()
		unless (defined $assemblies);
	# return entire list if no criteria are supplied
	my @assemblies = values $assemblies->%*;
	return @assemblies unless ($criteria->%*);
	# otherwise filter the runs by the criteria
	return RosenLab::filter(\@assemblies,$criteria,$reduce,0,"assembly");
}

=head1 RosenLab::Data::Genome::Annotation

Objectionable representation of a genome (transcriptome, etc.) annotation.

=cut

{
	package RosenLab::Data::Genome::Annotation;
	
	use experimental qw< signatures >;
	use namespace::autoclean;
	use overload q<""> => 'stringify';
	
	use File::Spec::Functions qw< catfile >;
	
	use Mouse;
	use Mouse::Util::TypeConstraints;
	no warnings qw< experimental::signatures >; # requried by/after Mouse
	
	use constant DATABASE =>
		catfile(RosenLab::Data::Genome::REPOSITORY,'annotations.yaml');
	
	sub import ()
	{
		my $yaml = YAML::PP::LoadFile(DATABASE);
		for my $name (keys $yaml->%*)
		{
			my $annotation = $yaml->{$name};
			$annotation->{'name'} = $name;
			$annotations->{$name} = __PACKAGE__->new($annotation);
		}
		return $annotations;
	}
	
	has 'name' =>
		(
			is => 'ro' ,
			isa => 'Str' ,
			required => 1 ,
		);
	has 'source' =>
		(
			is => 'ro' ,
			isa => 'Str' ,
			required => 1 ,
		);
	has 'assembly' =>
		(
			is => 'ro' ,
			isa => 'Str' ,
			required => 1 ,
		);
	has 'description' =>
		(
			is => 'ro' ,
			isa => 'Str' ,
			coerce => 1 ,
		);
	
	sub stringify : method ( $self , @others )
	{
		my $string = sprintf '%s{ ' , __PACKAGE__ ;
		$string .= join ' , ' , map
			{
				sprintf '%s => %s' , $_ , $self->$_ ;
			} qw< name source assembly >;
		return sprintf '%s }' , $string;
	}
	
	__PACKAGE__->meta->make_immutable;
}

=head1 RosenLab::Data::Genome::Assembly

Objectionable representation of a genome assembly.

=cut

{
	package RosenLab::Data::Genome::Assembly;
	
	use experimental qw< signatures >;
	use namespace::autoclean;
	use overload q<""> => 'stringify';
	
	use File::Spec::Functions qw< catfile >;
	
	use Mouse;
	use Mouse::Util::TypeConstraints;
	no warnings qw< experimental::signatures >;
	
	use constant DATABASE =>
		catfile(RosenLab::Data::Genome::REPOSITORY,'assemblies.yaml');
	
	sub import ()
	{
		my $yaml = YAML::PP::LoadFile(DATABASE);
		for my $name (keys $yaml->%*)
		{
			my $assembly = $yaml->{$name};
			$assembly->{'name'} = $name;
			$assemblies->{$name} = __PACKAGE__->new($assembly);
		}
		return $assemblies;
	}
	
	subtype 'RosenLab::Data::Genome::Assembly::AnnotationsList'
		=> as 'ArrayRef[RosenLab::Data::Genome::Annotation]';
	
	coerce 'RosenLab::Data::Genome::Assembly::AnnotationsList'
		=> from 'ArrayRef[Str]'
			=> via { [ map { RosenLab::Data::Genome::annotations({ 'name' => $_ }); } $_->@* ]; };
	
	has 'name' =>
		(
			is => 'ro' ,
			isa => 'Str' ,
			required => 1 ,
		);
	has 'source' =>
		(
			is => 'ro' ,
			isa => 'Str' ,
			required => 1 ,
		);
	has 'species' =>
		(
			is => 'ro' ,
			isa => 'Str' ,
			required => 1 ,
		);
	has 'annotations' =>
		(
			is => 'ro' ,
			isa => 'RosenLab::Data::Genome::Assembly::AnnotationsList' ,
			coerce => 1 ,
		);
	
	sub stringify : method ( $self , @others )
	{
		my $string = sprintf '%s{ ' , __PACKAGE__ ;
		$string .= join ' , ' , map
			{
				sprintf '%s => %s' , $_ , $self->$_ ;
			} qw< name source species >;
		return sprintf '%s }' , $string;
	}
	
	sub _build_annotations ( $self )
	{
		return [RosenLab::Data::Genome::find_annotations({ assembly => $self->name })];
	}
	
	__PACKAGE__->meta->make_immutable;
}

=head1 COPYRIGHT & LICENSE

Copyright (C) 2018 by Christopher Jacobs

Permission to use, copy, modify, and/or distribute this software for any purpose
with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF
THIS SOFTWARE.

=cut

1;

__DATA__

__END__
